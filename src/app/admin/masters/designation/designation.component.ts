import { Component, OnInit } from '@angular/core';
import { Validators } from "@angular/forms";
import { ViewChild } from '@angular/core'
import { FieldConfig } from "../../../interfaces/field.interface";
import { DynamicFormComponent } from '../../../components/dynamic-form/dynamic-form.component';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.css']
})
export class DesignationComponent implements OnInit {

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  formnameValue = {};
  constructor() {}

  ngOnInit(): void {
    console.log(this);
    console.log(this.form);
  }

  regConfig: FieldConfig[] = [
    {
      type: "input",
      label: "Designation Code",
      inputType: "text",
      name: "designationCode",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Code is Required"
        },
        {
          name: "pattern",
          validator: Validators.pattern("^[a-zA-Z]+$"),
          message: "Accept only text"
        }
      ]
    },{
      type: "input",
      label: "Designation Name",
      inputType: "text",
      name: "designationName",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Name is Required"
        },
        {
          name: "pattern",
          validator: Validators.pattern("^[a-zA-Z ]+$"),
          message: "Enter valid text."
        }
      ]
    },{
      type: "input",
      label: "Designation Description",
      inputType: "text",
      name: "designationDescription",
      validations: [
      ]
    },
    {
      type: "button",
      label: "Save"
    }
  ];

  submit(value: any) {
    console.log(value);
  }

}
