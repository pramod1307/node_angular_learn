import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesignationComponent } from './admin/masters/designation/designation.component';
import { PostCreateComponent } from './posts/post-create/post-create.component';
import { PostListCompoennt } from './posts/post-list/post-list.component';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/designation-create',
    pathMatch: 'full'
  },
  {
    path: 'admin-login',
    component: LoginComponent
  },
  {
    path: 'designation-create',
    component: DesignationComponent
  },
  {
    path: 'post-create',
    component: PostCreateComponent
  },
  {
    path: 'post-list',
    component: PostListCompoennt
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
