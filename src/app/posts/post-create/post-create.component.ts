import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Post } from '../post.model'
import { PostsService } from '../post.service';


@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {
  title = 'Post Create';
  newPost = 'No content added yet.';
  enteredPostTitle = '';
  enteredPostContent = '';
  //@Output() postCreated = new EventEmitter<Post>();

  constructor(public postService: PostsService) {

  }

  /*onAddPost(postInput) {
    this.newPost = postInput.value;
  }*/

  onAddPost(form: NgForm ) {
    if(form.invalid) {
      return;
    }

    //const post: Post = {
      /*title: this.enteredPostTitle,
      content: this.enteredPostContent*/
      //title: form.value.title,
      //content: form.value.content
    //};
    //this.postCreated.emit(post);
    this.postService.addPost(form.value.title, form.value.content);
    form.resetForm();
  };
}
