import { Component, OnInit , ViewChild} from '@angular/core';
import { Validators } from "@angular/forms";
import { FieldConfig } from "../../interfaces/field.interface";
import { DynamicFormComponent } from '../../components/dynamic-form/dynamic-form.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild(DynamicFormComponent) form: DynamicFormComponent;
  formnameValue = {};

  constructor() { }

  ngOnInit(): void {
  }

  loginFormConfig: FieldConfig[] = [
    {
      type: "input",
      label: "Username/Email",
      inputType: "text",
      name: "username",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Username is Required"
        },
        {
          name: "pattern",
          validator: Validators.pattern(
            "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"
          ),
          message: "Invalid email"
        }
      ]
    },{
      type: "input",
      label: "Password",
      inputType: "text",
      name: "password",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Password is Required"
        }
      ]
    },
    {
      type: "button",
      label: "Login"
    }
  ];

  loginSubmit(value: any) {
    console.log(value);
  }

}
